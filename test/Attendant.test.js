import ParkingLot from "../src/ParkingLot";
import {Attendant} from "../src/Attendant";
import {Car} from "../src/Car";
import {TrafficCop} from "../src/TrafficCop";
import {Owner} from "../src/Owner";
import {MaximumCapacity} from "../src/MaximumCapacity";
import {FirstAvailable} from "../src/FirstAvailable"

jest.mock("../src/Owner")
jest.mock("../src/TrafficCop")

describe('Attendant', () => {
  describe('park/unpark', () => {
    it('should park the car when park is called', () => {
      const parkingLot = new ParkingLot(2);
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant([parkingLot], parkingStrategy);
      const car = new Car();

      attendant.park(car);

      expect(parkingLot.hasCar(car)).toEqual(true);
    })
  })
  it('should unpark the car when unpark is called', () => {
    const parkingLot = new ParkingLot(2);
    const parkingStrategy = new FirstAvailable();
    const attendant = new Attendant([parkingLot], parkingStrategy);
    const car = new Car();

    attendant.park(car);
    attendant.unpark(car);

    expect(!parkingLot.hasCar(car)).toEqual(true);
  })
  describe('notify', () => {
    it('should park the car when park is called and notify attendant that'
        + ' parking lot is full', () => {
      const parkingLot = new ParkingLot(1);
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant([parkingLot], parkingStrategy);
      const notifyAttendant = jest.spyOn(attendant, 'notifyParkingLotFull');
      parkingLot.addObserver(attendant);
      const car = new Car();

      parkingLot.park(car);

      expect(notifyAttendant).toHaveBeenCalledTimes(1);
    })
    it('should notify all observers when parking lot is full'
        + ' parking lot is full', () => {
      const parkingLot = new ParkingLot(1);
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant([parkingLot], parkingStrategy);
      const owner = new Owner();
      const trafficCop = new TrafficCop();
      const notifyAttendant = jest.spyOn(attendant, 'notifyParkingLotFull');
      parkingLot.addObserver(attendant);
      parkingLot.addObserver(owner);
      parkingLot.addObserver(trafficCop);
      const car = new Car();

      parkingLot.park(car);

      expect(notifyAttendant).toHaveBeenCalledTimes(1);
      expect(owner.notifyParkingLotFull).toHaveBeenCalledTimes(1);
      expect(trafficCop.notifyParkingLotFull).toHaveBeenCalledTimes(1);
    })
  })
  describe('multiple parking lots', () => {
    it('should park the car in the first parking lot', () => {
      const firstParkingLot = new ParkingLot(1);
      const secondParkingLot = new ParkingLot(1);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const car = new Car();

      attendant.park(car);

      expect(firstParkingLot.hasCar(car)).toBe(true);

    })

    it('should park the second car in the second parking lot', () => {
      const firstParkingLot = new ParkingLot(1);
      const secondParkingLot = new ParkingLot(1);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const car = new Car();
      const anotherCar = new Car();

      attendant.park(car);
      attendant.park(anotherCar);

      expect(firstParkingLot.hasCar(car)).toBe(true);
      expect(secondParkingLot.hasCar(anotherCar)).toBe(true);
    });

    it('should park the second car in the second parking lot ', function () {
      const firstParkingLot = new ParkingLot(1);
      const secondParkingLot = new ParkingLot(1);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const car = new Car();
      const anotherCar = new Car();

      attendant.park(car);
      attendant.unpark(car);
      attendant.park(anotherCar);

      expect(firstParkingLot.hasCar(car)).toBe(false);
      expect(secondParkingLot.hasCar(anotherCar)).toBe(true);
    });

    it('should park the third car in the second parking lot ', function () {
      const firstParkingLot = new ParkingLot(1);
      const secondParkingLot = new ParkingLot(2);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const firstCar = new Car();
      const secondCar = new Car();
      const thirdCar = new Car();

      attendant.park(firstCar);
      attendant.unpark(firstCar);
      attendant.park(secondCar);
      attendant.park(thirdCar);

      expect(firstParkingLot.hasCar(firstCar)).toBe(false);
      expect(secondParkingLot.hasCar(secondCar)).toBe(true);
      expect(secondParkingLot.hasCar(secondCar)).toBe(true);
    });
  })

  describe('parking strategy', () => {
    it('should park the first car in the second parking lot based on maximum'
        + ' capacity', function () {
      const firstParkingLot = new ParkingLot(2);
      const secondParkingLot = new ParkingLot(3);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new MaximumCapacity();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const firstCar = new Car();
      const secondCar = new Car();
      const thirdCar = new Car();

      attendant.park(firstCar);
      attendant.park(secondCar);
      attendant.park(thirdCar)

      expect(secondParkingLot.hasCar(firstCar)).toBe(true);
      expect(firstParkingLot.hasCar(secondCar)).toBe(true);
      expect(secondParkingLot.hasCar(thirdCar)).toBe(true);
    });

    it('should throw error when all parking lots are full', function () {
      const firstParkingLot = new ParkingLot(1);
      const secondParkingLot = new ParkingLot(1);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new MaximumCapacity();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const firstCar = new Car();
      const secondCar = new Car();
      const thirdCar = new Car();

      attendant.park(firstCar);
      attendant.park(secondCar);


      expect(()=>{attendant.park(thirdCar)}).toThrow(Error('Parking'
          + ' Lot Full Exception'));
    });

    it('should park both cars in first parking lot', function () {
      const firstParkingLot = new ParkingLot(2);
      const secondParkingLot = new ParkingLot(5);
      const parkingLots = [firstParkingLot, secondParkingLot];
      const parkingStrategy = new FirstAvailable();
      const attendant = new Attendant(parkingLots, parkingStrategy);
      parkingLots.forEach((parkingLot) => {
        parkingLot.addObserver(attendant)
      })
      const firstCar = new Car();
      const secondCar = new Car();
      const thirdCar = new Car();

      attendant.park(firstCar);
      attendant.park(secondCar);

      expect(firstParkingLot.hasCar(firstCar)).toBe(true);
      expect(firstParkingLot.hasCar(secondCar)).toBe(true);
    });
  })
})
