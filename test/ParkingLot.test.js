import ParkingLot from "../src/ParkingLot";
import {Car} from "../src/Car";
import {Owner} from "../src/Owner"
import {TrafficCop} from "../src/TrafficCop"
import * as jest from "jest";

jest.mock("../src/Owner")
jest.mock("../src/TrafficCop")

describe('parking lot', () => {
  describe('#park/unpark', () => {
    beforeEach(() => {
      Owner.mockClear();
      TrafficCop.mockClear();
    });

    it('should park the car when car is parked in the parking lot', () => {
      const parkingLot = new ParkingLot(5);
      const car = new Car();

      parkingLot.park(car);

      expect(parkingLot.hasCar(car)).toEqual(true);
    })

    it('should not have the car 99when car is not parked in the parking lot',
        () => {
          const parkingLot = new ParkingLot(5);
          const car = new Car();

          expect(parkingLot.hasCar(car)).toEqual(false);
        })
    it('should not have the car when the car is parked and unparked in the'
        + ' parking lot',
        () => {
          const parkingLot = new ParkingLot(5);
          const car = new Car();

          parkingLot.park(car);
          parkingLot.unpark(car);

          expect(parkingLot.hasCar(car)).toEqual(false);
        })
    it('should return exception when when the car is not parked and'
        + ' unpark is tried',
        () => {
          const parkingLot = new ParkingLot(5);
          const car = new Car();

          expect(() => parkingLot.unpark(car)).toThrowError('Car not found'
              + ' Exception');
        })
    it('should return exception when when the car is parked and parking lot'
        + ' is full',
        () => {
          const trafficCop = new TrafficCop();
          const owner = new Owner();
          const parkingLot = new ParkingLot(1);
          parkingLot.addObserver(trafficCop);
          parkingLot.addObserver(owner);
          const car = new Car();

          parkingLot.park();

          expect(() => {
            parkingLot.park(car)
          }).toThrow(Error('Parking Lot Full Exception'));
        })
  })
  describe('notify', () => {
    it('should notify the observer when parking lot is full', () => {
      const trafficCop = new TrafficCop();
      const owner = new Owner();
      const parkingLot = new ParkingLot(2);
      parkingLot.addObserver(trafficCop);
      parkingLot.addObserver(owner);
      const car = new Car();
      const anotherCar = new Car();

      parkingLot.park(car);
      parkingLot.park(anotherCar);

      expect(owner.notifyParkingLotFull).toHaveBeenCalledTimes(1);

    })
    it('should notify the observer when parking lot is available', () => {
      const trafficCop = new TrafficCop();
      const owner = new Owner();
      const parkingLot = new ParkingLot(2);
      parkingLot.addObserver(trafficCop);
      parkingLot.addObserver(owner);
      const car = new Car();
      const anotherCar = new Car();

      parkingLot.park(car);
      parkingLot.park(anotherCar);
      parkingLot.unpark(car);

      expect(owner.notifyParkingLotAvailable).toHaveBeenCalledTimes(1);

    })
    it('should notify the traffic cop when parking lot is available', () => {
      const trafficCop = new TrafficCop();
      const owner = new Owner();
      const parkingLot = new ParkingLot(2);
      parkingLot.addObserver(trafficCop);
      parkingLot.addObserver(owner);
      const car = new Car();
      const anotherCar = new Car();

      parkingLot.park(car);
      parkingLot.park(anotherCar);
      parkingLot.unpark(car);

      expect(trafficCop.notifyParkingLotAvailable).toHaveBeenCalledTimes(1);
    })
    it('should notify the traffic cop when parking lot is full', () => {
      const trafficCop = new TrafficCop();
      const owner = new Owner();
      const parkingLot = new ParkingLot(2);
      parkingLot.addObserver(trafficCop);
      parkingLot.addObserver(owner);
      const car = new Car();
      const anotherCar = new Car();

      parkingLot.park(car);
      parkingLot.park(anotherCar);

      expect(trafficCop.notifyParkingLotFull).toHaveBeenCalledTimes(1);
    })
    it('should notify the traffic cop and owner when parking lot is full',
        () => {
          const trafficCop = new TrafficCop();
          const owner = new Owner();
          const parkingLot = new ParkingLot(2);
          parkingLot.addObserver(trafficCop);
          parkingLot.addObserver(owner);
          const car = new Car();
          const anotherCar = new Car();

          parkingLot.park(car);
          parkingLot.park(anotherCar);

          expect(trafficCop.notifyParkingLotFull).toHaveBeenCalledTimes(1);
          expect(owner.notifyParkingLotFull).toHaveBeenCalledTimes(1);
        })
    it('should notify the traffic cop and owner when parking lot is'
        + ' available', () => {
      const trafficCop = new TrafficCop();
      const owner = new Owner();
      const parkingLot = new ParkingLot(2);
      parkingLot.addObserver(trafficCop);
      parkingLot.addObserver(owner);
      const car = new Car();
      const anotherCar = new Car();

      parkingLot.park(car);
      parkingLot.park(anotherCar);
      parkingLot.unpark(car);

      expect(trafficCop.notifyParkingLotAvailable).toHaveBeenCalledTimes(1);
      expect(owner.notifyParkingLotAvailable).toHaveBeenCalledTimes(1);
    })
  })
})
