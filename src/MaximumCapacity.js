export class MaximumCapacity {
  selectParkingLot(parkingLots) {
    return parkingLots.reduce((parkingLot, anotherParkingLot) => {
       return parkingLot.compareCapacity(anotherParkingLot)
          ? parkingLot : anotherParkingLot;
    })
  }
}
