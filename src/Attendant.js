export class Attendant {
  constructor(parkingLots, parkingStrategy) {
    this.allParkingLots = parkingLots;
    this.availableParkingLots = parkingLots;
    this.parkingStrategy = parkingStrategy;
  }

  park(car) {
    if (this.availableParkingLots.length === 0) {
      throw Error('Parking Lot Full Exception');
    }
    const parkingLotToParkIn = this.parkingStrategy.selectParkingLot(
        this.availableParkingLots);
    parkingLotToParkIn.park(car);
  }

  unpark(car) {
    this.allParkingLots.forEach((parkingLot) => {
      if (parkingLot.hasCar(car)) {
        parkingLot.unpark(car);
      }
    })
  }

  notifyParkingLotFull(parkingLot) {
    this.availableParkingLots = this.availableParkingLots.filter(
        (availableParkingLot) => {
          return availableParkingLot !== parkingLot;
        })
  }

  notifyParkingLotAvailable(parkingLot) {
    this.availableParkingLots.push(parkingLot)
  }
}
