export default class ParkingLot {
  constructor(size) {
    this.capacity = size;
    this.parkingSpots = [];
    this.observers = [];
  }

  addObserver(observer) {
    this.observers.push(observer);
  }

  isFull() {
    return (this.parkingSpots.length === this.capacity);
  }

  park(car) {
    if (this.hasCar(car)) {
      throw Error('Car Already Parked Exception');
    }

    if (this.isFull()) {
      throw Error("Parking Lot Full Exception");
    }

    this.parkingSpots.push(car);

    if (this.isFull()) {
      this.observers.forEach((observer) => {
        observer.notifyParkingLotFull(this)
      });
    }
  }

  hasCar(car) {
    return this.parkingSpots.includes(car);
  }

  unpark(car) {
    if (!this.hasCar(car)) {
      throw Error('Car not found Exception')
    }

    if (this.isFull()) {
      this.observers.forEach((observer) => {
        observer.notifyParkingLotAvailable(this)
      });
    }

    this.parkingSpots = this.parkingSpots.filter((carInParkingLot) => {
      return !(carInParkingLot === car);
    })
  }

  compareCapacity(parkingLot){
    return (parkingLot.capacity - parkingLot.parkingSpots.length) <= (this.capacity - this.parkingSpots.length);
  }

}
